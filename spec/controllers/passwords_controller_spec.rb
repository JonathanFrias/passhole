require 'rails_helper'

RSpec.describe PasswordsController, type: :controller do

  let(:allowed_password) do
    Password.create(name: 'allowed', password: 'allowed')
  end

  let(:denied_password) do
    Password.create(name: 'denied', password: 'denied')
  end

  let(:allowed_passwords) { [allowed_password] }
  let(:denied_passwords) { [denied_password] }
  let(:all_passwords) do
    allowed_passwords + denied_passwords
  end

  let(:group) do
    Group.create name: 'group'
  end

  let(:current_user) do
    User.create name: 'user'
  end

  let(:group_access) do
    Membership.create(group: group, user: current_user)
  end

  let(:view_all_permission) do
    Permission.where(name: 'View All').first_or_create
  end

  let(:view_permission) do
    Permission.where(name: 'View').first_or_create
  end

  let(:other_user) do
    User.create name: 'other', email: "foobar"
  end

  let(:update_all_permission) do
    Permission.where(name: 'Update All').first_or_create
  end

  let(:update_permission) do
    Permission.where(name: 'Update').first_or_create
  end

  let(:upate_permission) do
    Permission.where(name: 'Update').first_or_create
  end

  let(:delete_permission) do
    Permission.where(name: 'Delete').first_or_create
  end

  let(:create_permission) do
    Permission.where(name: 'Create').first_or_create
  end

  let(:allowed_other_user) do
    [
      view_all_permission,
      view_permission,
      update_all_permission,
      update_permission,
      delete_permission,
      create_permission,
    ].each do |permission|
      Membership.create(
          permission: permission,
          user: other_user,
          password: allowed_password)
    end
  end

  context "#unauthenticated" do
    it "does not load unauthenticated users" do
      get :index
      assert_redirected_to new_user_session_path
    end

    it "does not load show page" do
      get :show, params: { id: denied_password.id }
      assert_redirected_to new_user_session_path
    end

    it "does not load edit page" do
      get :edit, params: { id: denied_password.id }
      assert_redirected_to new_user_session_path
    end

    it "does not load new page" do
      get :new, params: { id: denied_password.id }
      assert_redirected_to new_user_session_path
    end

    it "does not do an update" do
      put :update, params: { id: denied_password.id }
      assert_redirected_to new_user_session_path
    end

    it "does not do destroy" do
      delete :destroy, params: { id: denied_password.id }
      assert_redirected_to new_user_session_path
    end

    it "does not create" do
      post :create
      assert_redirected_to new_user_session_path
    end
  end

  context "#index" do
    before do
      sign_in current_user
    end

    it "cannot list the passwords" do
      get :index
      expect(assigns[:passwords]).to eq []
    end

    it "can list the passwords through the direct 'View All' permission" do
      all_passwords
      Membership.create user: current_user, permission: view_all_permission
      get :index
      expect(assigns[:passwords].count).to eq all_passwords.count
    end

    it "lists all passwords via the group 'View All' permission" do
      all_passwords
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: view_all_permission
      get :index
      expect(assigns[:passwords].count).to eq all_passwords.count
    end

    it "list specific password by group 'View' permission" do
      all_passwords
      Membership.create user: current_user, group: group
      Membership.create group: group, password: allowed_password, permission: view_permission
      get :index
      expect(assigns[:passwords].count).to eq 1
      expect(assigns[:passwords].first.id).to eq allowed_password.id
    end

    it "list specific password via direct membership permission" do
      all_passwords
      Membership.create user: current_user, password: allowed_password
      get :index
      expect(assigns[:passwords].count).to eq 1
      expect(assigns[:passwords].first.id).to eq allowed_password.id
    end

    it "fetches password when the permission has the password" do
      all_passwords
      Membership.create user: current_user, permission: view_permission
      Membership.create permission: view_permission, password: allowed_password
      get :index
      expect(assigns[:passwords].count).to eq 1
      expect(assigns[:passwords].first.id).to eq allowed_password.id
    end
  end

  context "#show" do
    before do
      sign_in current_user
    end

    it "does not show denied passwords" do
      denied_password
      get :show, params: { id: denied_password.id }
      expect(assigns[:password]).to eq nil
      assert_redirected_to root_path
    end

    it "shows the password via the 'View All' permission" do
      Membership.create user: current_user, permission: view_all_permission
      all_passwords.each do |pass|
        get :show, params: { id: pass.id }
        expect(assigns[:password]).to eq pass
      end
    end

    it "shows the password via the group 'View All' permission" do
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: view_all_permission
      get :show, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end

    it "shows the password via the group password permission" do
      Membership.create user: current_user, group: group
      Membership.create group: group, password: allowed_password, permission: view_permission
      get :show, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end

    it "shows the password via the direct password permission" do
      Membership.create user: current_user, password: allowed_password, permission: view_permission
      get :show, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end

    it "Can handle complex memberships properly" do
      Membership.create user: current_user, password: allowed_password, permission: view_permission
      get :show, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end

    it "shows the passwords that belongs to the permission" do
      Membership.create user: current_user, permission: view_permission
      Membership.create permission: view_permission, password: allowed_password
      get :show, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end
  end

  context "#edit" do
    before do
      sign_in current_user
    end

    it "does not edit denied passwords" do
      denied_password
      get :edit, params: { id: denied_password.id }
      expect(assigns[:password]).to eq nil
      assert_redirected_to root_path
    end

    it "edits the password via the 'Update All' permission" do
      Membership.create user: current_user, permission: update_all_permission
      all_passwords.each do |pass|
        get :edit, params: { id: pass.id }
        expect(assigns[:password]).to eq pass
      end
    end

    it "edits the password via the specific group edit permission" do
      all_passwords
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: upate_permission, password: allowed_password
      get :edit, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end

    it "edits the password via the group 'Edit All' permission" do
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: update_all_permission
      get :edit, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end

    it "edits the password via the group password permission" do
      Membership.create user: current_user, group: group
      Membership.create group: group, password: allowed_password, permission: update_permission
      get :edit, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end

    it "edits the password via the direct password permission" do
      Membership.create user: current_user, password: allowed_password
      get :edit, params: { id: allowed_password.id }
      expect(assigns[:password]).to eq allowed_password
    end
  end

  context "#new" do
    before do
      sign_in current_user
    end

    it "cannot create a new user without the permission set" do
      get :new
      assert_redirected_to root_path
    end

    it "can load the form with the 'Create' permission set" do
      Membership.create user: current_user, permission: create_permission
      get :new
      assert_response :ok
    end

    it "can load the form with the group 'Create' permission set" do
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: create_permission
      get :new
      assert_response :ok
    end

    it "can load the form with the group 'Create' permission set" do
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: create_permission
      get :new
      assert_response :ok
    end
  end

  context "#create" do
    before do
      sign_in current_user
    end

    let(:create_permission) do
      Permission.where(name: 'Create').first_or_create
    end

    let(:attempt_create) do
      post :create, params: { password: { name: 'name', password: 'password' } }
    end

    it "can create via the 'Create' permission" do
      Membership.create user: current_user, permission: create_permission
      expect {
        attempt_create
      }.to change { Password.count }.by(1)
    end

    it "can create via the group 'Create' permission" do
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: create_permission
      expect {
        attempt_create
      }.to change { Password.count }.by(1)
    end
  end

  context "#update" do
    before do
      sign_in current_user
    end

    let(:attempt_update) do
      put :update, params: { id: allowed_password.id, password: { name: 'updated_name', password: '1234' } }
      expect(allowed_password.reload.name).to eq 'updated_name'
      expect(allowed_password.reload.password).to eq '1234'
    end

    it "can update via the 'Update All' permission" do
      Membership.create user: current_user, permission: update_all_permission
      attempt_update
    end

    it "can update via the group 'Update All' permission" do
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: update_all_permission
      attempt_update
    end

    it "can update via the direct permission" do
      Membership.create user: current_user, permission: update_permission, password: allowed_password
      attempt_update
    end

    it "can update via the group permission" do
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: update_permission, password: allowed_password
      attempt_update
    end

    it "can update user's direct passwords" do
      Membership.create user: current_user, password: allowed_password
      attempt_update
    end
  end

  context "#delete" do
    before do
      sign_in current_user
    end

    let(:attempt_delete) do
      delete :destroy, params: { id: allowed_password.id }
    end

    it "can delete via the 'Delete' permission" do
      allowed_password
      Membership.create user: current_user, permission: delete_permission
      expect {
        attempt_delete
      }.to change { Password.count }.by(-1)
    end

    it "can delete via the group 'Delete' permission" do
      Membership.create user: current_user, group: group
      Membership.create group: group, permission: delete_permission
      allowed_password
      expect {
        attempt_delete
      }.to change { Password.count }.by(-1)
    end
  end
end
