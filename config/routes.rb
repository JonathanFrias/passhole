Rails.application.routes.draw do
  devise_for :users, {
    sessions: 'users/sessions'
  }

  resources :passwords
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root 'passwords#index'
  namespace :admin do
    resources :groups
    resources :memberships
    resources :permissions
    resources :roles
    resources :users
  end
  # resources :roles
  # resources :groups
  # resources :permissions
  # resources :users
end
