class CurrentSchema < ActiveRecord::Migration[5.2]
  def change
    # These are extensions that must be enabled in order to support this database
    enable_extension "plpgsql"

    create_table "groups", force: :cascade do |t|
      t.string "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "memberships", force: :cascade do |t|
      t.integer "user_id"
      t.integer "group_id"
      t.integer "permission_id"
      t.integer "password_id"
    end

    create_table "passwords", force: :cascade do |t|
      t.string "name"
      t.string "password"
    end

    create_table "permissions", force: :cascade do |t|
      t.string "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "users", force: :cascade do |t|
      t.string "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "email", default: "", null: false
      t.string "encrypted_password", default: "", null: false
      t.string "reset_password_token"
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.string "password"
      t.index ["email"], name: "index_users_on_email", unique: true
      t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    end

    add_foreign_key "memberships", "groups"
    add_foreign_key "memberships", "permissions"
    add_foreign_key "memberships", "users"
  end
end
