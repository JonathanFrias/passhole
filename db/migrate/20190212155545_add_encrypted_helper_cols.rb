class AddEncryptedHelperCols < ActiveRecord::Migration[5.2]
  def change
    add_column(:passwords, :encrypted_password, :string)
    add_column(:passwords, :encrypted_password_iv, :string)
    remove_column(:passwords, :password, :string)
  end
end
