class AddPasswordsConstraint < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key "memberships", "passwords"
  end
end
