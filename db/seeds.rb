# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# CREATES
Permission.where(name: 'Create').first_or_create

# READS
Permission.where(name: 'View All').first_or_create
Permission.where(name: 'View').first_or_create

# UPDATES
Permission.where(name: 'Update All').first_or_create
Permission.where(name: 'Update').first_or_create

# DELETES
Permission.where(name: 'Delete').first_or_create

# APP
admin = Permission.where(name: 'Admin').first_or_create


u = User.create name: 'Admin', email: 'admin', password: '12345678'
u.permissions << admin
