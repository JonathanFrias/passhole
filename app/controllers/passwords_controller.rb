class PasswordsController < ApplicationController
  before_action :authenticate_user!

  # GET /passwords
  # GET /passwords.json
  def index
    list_all_permission = Permission.where(name: 'View All').first_or_create
    view_permission = Permission.where(name: 'View').first_or_create

    return @passwords = Password.all if current_user.permissions.include?(list_all_permission)
    return @passwords = Password.all if current_user.all_permissions.include?(list_all_permission)

    @passwords =
      Membership.where("
        group_id in (:group_id )
        or (user_id = :user_id AND permission_id = :view_permission_id)
        or (permission_id in (:permission_list))
        or (user_id = :user_id)
        ",
        {
          group_id: current_user.groups.select(:id),
          user_id: current_user.id,
          view_permission_id: view_permission.id,
          permission_list: current_user.all_permissions.map(&:id)
        }
      ).map { |x| x.password }.compact.uniq
  end

  # GET /passwords/1
  # GET /passwords/1.json
  def show
    list_all_permission = Permission.where(name: 'View All').first_or_create
    view_permission = Permission.where(name: 'View').first_or_create
    return @password = current_password if current_user.all_permissions.include?(list_all_permission)
    @password = Membership.where("(
        group_id in (:group_id )
        or (user_id = :user_id AND permission_id = :view_permission_id)
        or (permission_id in (:permission_list))
        or (user_id = :user_id))
        AND (password_id = :password_id)
      ",
      {
        group_id: current_user.groups.select(:id),
        user_id: current_user.id,
        view_permission_id: view_permission.id,
        permission_list: current_user.all_permissions.map(&:id),
        password_id: current_password.id,
      }
    ).map { |x| x.password }.compact.uniq.first
    redirect_back fallback_location: root_path if @password.nil?
  end

  # GET /passwords/new
  def new
    create_permission = Permission.where(name: 'Create').first_or_create
    if(current_permissions.include?(create_permission))
      @password = Password.new
    else
      redirect_back fallback_location: root_path
    end
  end

  # GET /passwords/1/edit
  def edit
    update_all_permission = Permission.where(name: 'Update All').first_or_create
    update_permission = Permission.where(name: 'Update').first_or_create

    return set_password if current_user.all_permissions.include?(update_all_permission)

    @password = Membership.where("
      group_id in (:group_id)
      or (user_id = :user_id AND permission_id = :permission_id)
      or (user_id = :user_id AND password_id = :password_id)
    ",
    {
      permission_id: update_permission.id,
      user_id: current_user.id,
      group_id: current_user.groups.select(:id),
      password_id: current_password.id,
    }).map { |x| x.password }.compact.uniq.first

    return redirect_back fallback_location: root_path unless @password
  end

  # POST /passwords
  # POST /passwords.json
  def create
    create_permission  = Permission.where(name: 'Create').first_or_create
    unless (current_permissions.include?(create_permission))
      return head 403
    end

    @password = Password.new(password_params)

    respond_to do |format|
      if @password.save
        Membership.create user: current_user, password: @password
        format.html { redirect_to @password, notice: 'Password was successfully created.' }
        format.json { render :show, status: :created, location: @password }
      else
        format.html { render :new }
        format.json { render json: @password.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /passwords/1
  # PATCH/PUT /passwords/1.json
  def update
    update_all_permission  = Permission.where(name: 'Update All').first_or_create
    update_permission  = Permission.where(name: 'Update').first_or_create

    @password = Membership.where("
      group_id in (:group_ids)
      or (permission_id = :permission_id)
      or (user_id = :user_id)
      ", {
        permission_id: update_all_permission.id,
        group_ids: current_user.groups.select(:id),
        user_id: current_user.id,
      }
    ).map { |x| x.password }.compact.uniq.first

    set_password if current_permissions.include?(update_all_permission)
    respond_to do |format|
      if @password.update(password_params)
        format.html { redirect_to @password, notice: 'Password was successfully updated.' }
        format.json { render :show, status: :ok, location: @password }
      else
        format.html { render :edit }
        format.json { render json: @password.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /passwords/1
  # DELETE /passwords/1.json
  def destroy
    delete_permission  = Permission.where(name: 'Delete').first_or_create
    unless (current_permissions.include?(delete_permission))
      return head 403
    end
    current_password.destroy
    respond_to do |format|
      format.html { redirect_to passwords_url, notice: 'Password was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_password
      @password = current_password
    end

    def current_password
      Password.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def password_params
      params.require(:password).permit(:name, :password)
    end

    def current_permissions
      current_user.all_permissions
    end
end
