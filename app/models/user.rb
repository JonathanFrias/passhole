class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  devise :database_authenticatable
  # devise :ldap_authenticatable # :registerable,
         # :recoverable, :rememberable #, :validatable
  has_many :memberships, dependent: :destroy
  has_many :groups, through: :memberships
  has_many :permissions, through: :memberships
  has_many :passwords, through: :memberships
  before_destroy :clear_memberships

  def clear_memberships
    memberships.where(user_id: self.id).destroy_all
  end

  def ldap_before_save
    user = User.where(email: self.email + "@rlcs.local").first
    temp = User.where(email: self.email).first || User.new(email: self.email)
    temp.save(validate: false)
    self.id = user.id
    self.instance_variable_set(:@new_record, false)
    self.email = self.email + "@rlcs.local"
    binding.pry
  end

  def all_permissions
    permissions + groups.map do |it|
      it.permissions
    end.flatten.uniq
  end

  def is_admin?
    admin_permission = Permission.where(name: 'Admin').first
    all_permissions.include?(admin_permission)
  end
end
