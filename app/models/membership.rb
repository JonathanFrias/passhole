class Membership < ApplicationRecord
  belongs_to :user,       optional: true
  belongs_to :permission, optional: true
  belongs_to :group,      optional: true
  belongs_to :permission, optional: true
  belongs_to :password,   optional: true
end
