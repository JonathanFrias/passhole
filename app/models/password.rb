class Password < ApplicationRecord
  attr_encrypted :password, key: Rails.application.credentials[:password_key]
  has_many :memberships
  has_many :permissions, through: :memberships
  has_many :users, through: :memberships
  has_many :groups, through: :memberships

  before_destroy :clear_memberships

  def clear_memberships
    memberships.each do |membership|
      membership.update password_id: nil
    end
  end

  def all_users
    users + groups.map do |it|
      it.users
    end.flatten.uniq
  end
end
