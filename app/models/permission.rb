class Permission < ApplicationRecord
  has_many :memberships, dependent: :destroy
  has_many :groups, through: :memberships
  has_many :users, through: :memberships
  has_many :passwords, through: :memberships
  before_destroy :clear_memberships

  def clear_memberships
    memberships.where(permission_id: self.id).destroy_all
  end
end
