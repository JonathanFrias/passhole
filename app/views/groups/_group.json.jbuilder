json.extract! group, :id, :permission_id, :role_id, :created_at, :updated_at
json.url group_url(group, format: :json)
