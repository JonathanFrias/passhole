json.extract! password, :id, :name, :password, :created_at, :updated_at
json.url password_url(password, format: :json)
