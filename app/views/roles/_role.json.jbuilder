json.extract! role, :id, :group_id, :permission_id, :role_id, :created_at, :updated_at
json.url role_url(role, format: :json)
